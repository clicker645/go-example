package components

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
)

type Lot struct {
	sync.Mutex

	CurrentBid int // текущая ставка
	PlayerId   int // номер игрока, которйы сделал ставку
	MaxPrice   int // максимальная цена лота
	MaxBids    int // максимальная ставка
	CurrentCnt int // текущие количесвто ставок
	Winner     Player
}

func NewLot(maxPrice int, maxBids int, currentBid int) *Lot {
	return &Lot{MaxPrice: maxPrice, MaxBids: maxBids, CurrentBid: currentBid}
}

func (b *Lot) SetNewBid(NewBid Player) bool {
	b.Lock()
	defer b.Unlock()

	if b.CurrentBid < NewBid.Bid {
		fmt.Printf("player %d, bid %d \n", NewBid.PlayerId, NewBid.Bid)
		b.CurrentBid = NewBid.Bid
		b.PlayerId = NewBid.PlayerId
		b.CurrentCnt++

		if b.MaxPrice <= b.CurrentBid || b.MaxBids <= b.CurrentCnt {
			fmt.Println("finish by price")
			b.Winner = NewBid
			return true
		}
	}

	return false
}

func (b *Lot) MakeBid(ctx context.Context, playerId int, bids chan Player) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		myBid := b.CurrentBid + rand.Intn(AvgBidSteep)
		select {
		case <-ctx.Done():
			return
		case bids <- Player{PlayerId: playerId, Bid: myBid}:
		default:
		}
	}
}
