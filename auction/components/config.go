package components

const (
	AvgBidSteep              = 100
	DefaultMaxPrice          = 1000
	DefaultMaxCountBids      = 50
	DefaultCurrentBid        = 100
	AuctionDurationInSeconds = 20
)
