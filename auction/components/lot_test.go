package components

import (
	"context"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

func TestNewLot(t *testing.T) {
	Test := []struct {
		maxPrice    int
		maxBids     int
		currentBind int
	}{
		{1000, 50, 100},
		{2000, 80, 100},
		{40000, 20, 100},
		{1212, 10, 100},
	}
	for _, val := range Test {
		obj := NewLot(val.maxPrice, val.maxBids, val.currentBind)
		assert.Equal(t, &Lot{MaxPrice: val.maxPrice, MaxBids: val.maxBids, CurrentBid: val.currentBind}, obj, "False created new Lot")
	}
}

func TestLot_SetNewBid(t *testing.T) {
	p := &Player{
		1, 120,
	}
	lot := NewLot(DefaultMaxPrice, DefaultMaxCountBids, DefaultCurrentBid)
	for {
		r := lot.SetNewBid(*p)
		if r == false {
			assert.False(t, r, "Error set new bid")
			p.Bid = p.Bid + rand.Intn(AvgBidSteep)
		} else {
			assert.True(t, r, "Error set new bid")
			break
		}
	}
}

func TestLot_MakeBid(t *testing.T) {
	bids := make(chan Player)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*AuctionDurationInSeconds)

	lot := NewLot(DefaultMaxPrice, DefaultMaxCountBids, DefaultCurrentBid)

	for i := 0; i < 5; i++ {
		go lot.MakeBid(ctx, i, bids)
	}

LoopLot:
	for {
		select {
		case <-ctx.Done():
			break LoopLot
		case bid := <-bids:
			assert.NotNil(t, bid, "Bid don`t created")
			cancel()
		}
	}

}
