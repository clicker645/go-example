package main

import (
	"context"
	"fmt"
	"go-example/auction/components"
	"time"
)

func main() {

	bids := make(chan components.Player)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*components.AuctionDurationInSeconds)

	lot := components.NewLot(components.DefaultMaxPrice, components.DefaultMaxCountBids, components.DefaultCurrentBid)

	for i := 0; i < 5; i++ {
		go lot.MakeBid(ctx, i, bids)
	}

LoopLot:
	for {
		select {
		case <-ctx.Done():
			break LoopLot
		case bid := <-bids:
			if lot.SetNewBid(bid) {
				cancel()
			}
		}
	}

	fmt.Println("WINNER", lot.Winner)
}
