package client

import (
	"bufio"
	"fmt"
	"go-example/tcp/config"
	"net"
	"os"
)

type Client struct {
	cfg *config.Config
}

func NewClient(cfg *config.Config) *Client {
	return &Client{
		cfg: cfg,
	}
}

func (c *Client) Dial() (err error) {
	conn, err := net.Dial(c.cfg.Type, fmt.Sprintf("%s:%s", c.cfg.Host, c.cfg.Port))
	if err != nil {
		return
	}

	input := bufio.NewReader(os.Stdin)
	go c.Response(conn)

	var userLine string

	for {
		userLine, err = input.ReadString('\n')
		if err != nil {
			return
		}

		conn.Write([]byte(userLine))

	}
	return
}

func (c *Client) Response(conn net.Conn) (err error) {
	response := bufio.NewReader(conn)
	var serverLine string
	for {
		serverLine, err = response.ReadString('\n')
		if err != nil {
			return
		}

		fmt.Printf("response server ---> %v", serverLine)
	}
}
