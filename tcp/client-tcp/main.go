package main

import (
	"go-example/tcp/client-tcp/client"
	"go-example/tcp/config"
)

func main() {

	c := client.NewClient(&config.Config{
		Type: config.ConnType,
		Host: config.ConnHost,
		Port: config.ConnPort,
	})

	c.Dial()

}
