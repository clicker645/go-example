package config

const (
	ConnHost = "127.0.0.1"
	ConnPort = "8001"
	ConnType = "tcp"
)

type Config struct {
	Type string
	Host string
	Port string
}
