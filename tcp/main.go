package main

import (
	"go-example/tcp/config"
	"go-example/tcp/server-tcp"
)

func main() {
	s := server_tcp.NewServer(&config.Config{
		Type: config.ConnType,
		Host: config.ConnHost,
		Port: config.ConnPort,
	})

	if err := s.Listen(); err != nil {
		panic(err)
	}
}
