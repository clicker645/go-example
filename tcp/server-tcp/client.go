package server_tcp

import (
	"bufio"
	"net"
)

type Client struct {
	Broadcaster

	conn net.Conn
}

func (c *Client) run() (err error) {

	var (
		r    = bufio.NewReader(c.conn)
		line string
	)

	for {
		line, err = r.ReadString('\n')
		if err != nil {
			return
		}

		err = c.Emit(line)
		if err != nil {
			return
		}
	}

	return
}
