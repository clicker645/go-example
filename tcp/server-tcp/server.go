package server_tcp

import (
	"fmt"
	"go-example/tcp/config"
	"net"
	"sync"
)

type Server struct {
	sync.Mutex
	*Broadcast

	cfg *config.Config
}

func NewServer(cfg *config.Config) *Server {
	return &Server{
		Broadcast: new(Broadcast),
		cfg:       cfg,
	}
}

func (s *Server) Listen() (err error) {
	l, err := net.Listen(s.cfg.Type, fmt.Sprintf("%s:%s", s.cfg.Host, s.cfg.Port))
	if err != nil {
		return
	}

	defer func() {
		err = l.Close()
	}()

	var conn net.Conn
	for {
		conn, err = l.Accept()
		if err != nil {
			return
		}

		c := s.NewClient(conn)
		s.AddClient(conn)

		go func() {
			err = c.run()
		}()
	}
}

func (s *Server) NewClient(conn net.Conn) *Client {
	return &Client{
		Broadcaster: s.Broadcast,
		conn:        conn,
	}
}

func (s *Server) AddClient(c net.Conn) {
	s.Lock()
	defer s.Unlock()

	s.conns = append(s.conns, c)
}
