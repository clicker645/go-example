package server_tcp

import (
	"fmt"
	"net"
	"sync"
)

type Broadcaster interface {
	Emit(string) error
}

type Broadcast struct {
	sync.Mutex

	conns []net.Conn
}

func (b *Broadcast) Emit(msg string) error {
	b.Lock()
	defer b.Unlock()
	fmt.Println(b.conns)
	for _, c := range b.conns {
		c.Write([]byte(msg))
	}

	return nil
}
