package main

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
)

func Call(fn interface{}, argsFn ...interface{}) {
	v := reflect.ValueOf(fn)
	t := reflect.TypeOf(fn)
	if v.Kind() != reflect.Func {
		panic("It`s not a function")
	}

	rArgsFn := make([]reflect.Value, len(argsFn))
	argv := make([]reflect.Value, len(argsFn))
	for i := range argsFn {
		argv[i] = reflect.New(t.In(i)).Elem()
		rArgsFn[i] = reflect.ValueOf(argsFn[i])
	}

	for idx, val := range rArgsFn {
		arg := argv[idx]
		switch val.Kind() {
		case reflect.String:
			switch arg.Kind() {
			case reflect.String:
				arg.SetString(val.String())
			default:
				arg := reflect.New(arg.Type())
				err := json.Unmarshal([]byte(val.String()), arg.Interface())
				if err != nil {
					log.Fatal(err)
				}

				argv[idx] = arg.Elem()
			}
		case reflect.Struct, reflect.Ptr:
			arg.Set(val)
		case reflect.Int64, reflect.Int:
			arg.SetInt(val.Int())
		}
	}

	v.Call(argv)
}

type User struct {
	Name string
}

func main() {
	var mapFn = make(map[string]interface{})

	mapFn["int"] = func(i *int, j int) {
		fmt.Println("this is integer value ->", i, j)
	}

	mapFn["intPtr"] = func(i *int) {
		fmt.Println("this is integer value ->", i)
	}

	mapFn["string"] = func(s string) {
		fmt.Println("this is string value ->", s)
	}

	mapFn["ptrStruct"] = func(user *User) {
		fmt.Println("this is pointer to struct value ->", user.Name)
	}

	mapFn["struct"] = func(user User) {
		fmt.Println("this is struct value ->", user.Name)
	}

	var value interface{}

	value = `{"name": "Bob"}`
	//value = User{Name: "Stig"}
	//value = &User{Name: "Stig"}
	//Call(mapFn["int"], a, 2)
	//Call(mapFn["intPtr"], &a)
	//Call(mapFn["string"], value)
	//Call(mapFn["ptrStruct"], value)
	Call(mapFn["struct"], value)
}
