package library_dir

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewLibrary(t *testing.T) {
	assert.NotNil(t, NewLibrary(), "Library not created")
}

func TestLibrary_CreateBooks(t *testing.T) {
	var l = &Library{}
	l.CreateBooks(StartIndexingBook, BookCount)
	assert.NotNil(t, l.Books, "Books not created")
}

func TestLibrary_GetCountBook(t *testing.T) {
	var l = &Library{}
	l.CreateBooks(StartIndexingBook, BookCount)
	c := l.GetCountBook()
	q := BookCount - StartIndexingBook
	assert.Equal(t, q, c, "False quantity  getting books")
}

func TestLibrary_GiveBook(t *testing.T) {
	var l = &Library{}
	l.CreateBooks(StartIndexingBook, BookCount)
	Test := []struct {
		personId       int
		completedBooks []int
	}{
		{1, []int{1, 2}},
		{2, []int{2, 1, 3, 4, 5}},
	}

	b := l.GiveBook(Test[0].personId, Test[0].completedBooks).BookId
	r := IsRead(Test[0].completedBooks, b)
	assert.True(t, r, "Test failed-> expected is not book of %v, received: %v", Test[0].completedBooks, b)

	book := l.GiveBook(Test[1].personId, Test[1].completedBooks)
	assert.Nil(t, book, "Test failed-> expected is not book of %v, received: %v", Test[0].completedBooks, b)
}

func TestIsRead(t *testing.T) {
	Test := []struct {
		entry int
		slice []int
	}{
		{1, []int{2, 3, 4}},
		{2, []int{3, 1, 2}},
	}

	assert.True(t, IsRead(Test[0].slice, Test[0].entry), "func return false, but need return true")
	assert.False(t, IsRead(Test[1].slice, Test[1].entry), "func return true, but need return false")

}

func TestLibrary_ReturnBook(t *testing.T) {
	var l = &Library{}
	l.CreateBooks(StartIndexingBook, BookCount)
	Test := []struct {
		personId       int
		completedBooks []int
	}{
		{1, []int{1, 2}},
	}

	b := l.GiveBook(Test[0].personId, Test[0].completedBooks).BookId
	l.ReturnBook(b, Test[0].personId)

	for index, value := range l.Books {
		if value.BookId == b && value.Reader == Test[0].personId {
			assert.True(t, l.Books[index].Reader == 0, "Error return book in library_dir")
		}
	}

}

func BenchmarkLibrary_CreateBooks(b *testing.B) {
	l := NewLibrary()

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		l.CreateBooks(StartIndexingBook, BookCount)
	}
}

func BenchmarkLibrary_GetCountBook(b *testing.B) {
	l := NewLibrary()
	l.CreateBooks(StartIndexingBook, BookCount)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		l.GetCountBook()
	}
}

func BenchmarkNewLibrary(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		NewLibrary()
	}
}

func BenchmarkIsRead(b *testing.B) {
	l := NewLibrary()
	l.CreateBooks(StartIndexingBook, BookCount)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		IsRead([]int{i + 1, i + 2, i + 3}, i)
	}
}

func BenchmarkLibrary_GiveBook(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()

	var l = &Library{}
	l.CreateBooks(StartIndexingBook, BookCount)
	Test := struct {
		personId       int
		completedBooks []int
	}{
		2, []int{2, 1},
	}

	for i := 0; i < b.N; i++ {
		l.GiveBook(Test.personId, Test.completedBooks)
	}
}
