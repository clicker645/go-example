package library_dir

import (
	"go-example/library/book"
	"io/ioutil"
	"log"
	"strconv"
	"sync"
)

const (
	BookCount         = 6
	StartIndexingBook = 1
	PathToBooks       = "../../../../"
)

type Library struct {
	sync.Mutex

	countBookCache int8
	Books          []*book.Book
}

func NewLibrary() *Library {
	return &Library{Books: make([]*book.Book, BookCount, BookCount)}
}

func (l *Library) CreateBooks(startIndexing int, bookCount int) {
	for i := startIndexing; i < bookCount; i++ {
		idxStr := strconv.Itoa(i)
		b, err := ioutil.ReadFile(PathToBooks + book.BookName + idxStr + "." + book.BookFormat)
		if err != nil {
			log.Fatal(err)
		}

		l.Books = append(l.Books, book.NewBook(i, b))
	}

	l.countBookCache = int8(len(l.Books))
}

func (l *Library) GiveBook(personId int, completedBooks []int) *book.Book {
	l.Lock()
	for index, book := range l.Books {
		if book.Reader == 0 && IsRead(completedBooks, book.BookId) {
			l.Books[index].Reader = personId
			l.Unlock()
			return book
		}
	}
	l.Unlock()

	return nil
}

func (l *Library) ReturnBook(bookId int, personId int) {
	l.Lock()
	for index, value := range l.Books {
		if value.BookId == bookId && value.Reader == personId {
			l.Books[index].Reader = 0
		}
	}
	l.Unlock()
}

func (l *Library) GetCountBook() int {
	return int(l.countBookCache)
}

func IsRead(s []int, i int) bool {
	for _, v := range s {
		if v == i {
			return false
		}
	}

	return true
}
