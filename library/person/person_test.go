package person

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go-example/library/library_dir"
	"os"
	"testing"
	"time"
)

var l = &library_dir.Library{}

func TestMain(m *testing.M) {
	l.CreateBooks(library_dir.StartIndexingBook, library_dir.BookCount)
	os.Exit(m.Run())
}

func TestNewPerson(t *testing.T) {
	Test := []struct {
		personId int
		Person
	}{
		{1, Person{PersonId: 1}},
		{2, Person{PersonId: 2}},
		{3, Person{PersonId: 3}},
		{4, Person{PersonId: 4}},
	}
	for _, val := range Test {
		assert.Equal(t, *NewPerson(val.PersonId), val.Person, "False created Person structure")
	}
}

func TestPerson_TakeBook(t *testing.T) {
	var (
		p      = Person{PersonId: 1}
		ctx, _ = context.WithCancel(context.Background())
		c      = make(chan *Person, 1)
	)

	p.TakeBook(l, ctx, c)
	assert.Equal(t, &p, <-c, "False take book")
}

func TestPerson_Read(t *testing.T) {
	p := Person{PersonId: 1}
	p.Read(l.Books[0])
	assert.Equal(t, p.LastWordsInBook[0].Word, 1, "incorrect reading new book")
	p.Read(l.Books[0])
	assert.Equal(t, p.LastWordsInBook[0].Word, 2, "incorrect read continuation")
	p.Read(l.Books[0])
	for {
		p.Read(l.Books[0])
		if len(l.Books[0].Text) == p.LastWordsInBook[0].Word+1 {
			p.Read(l.Books[0])

			break
		}
	}

	assert.Equal(t, p.LastWordsInBook[0].Word, len(l.Books[0].Text), "book read but not added to read list")
}

func TestCheckNewBook(t *testing.T) {
	Test := []struct {
		entry int
		s     []ReadableBooks
	}{
		{4, []ReadableBooks{
			{1, 2},
			{2, 2},
			{3, 2},
		}},
		{1, []ReadableBooks{
			{4, 2},
			{2, 2},
			{3, 2},
		}},
	}

	for _, val := range Test {
		assert.True(t, CheckNewBook(val.s, val.entry), "func return false, but need return true")
	}
}

func BenchmarkCheckNewBook(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		CheckNewBook([]ReadableBooks{
			{1, 1},
		}, i)
	}
}

func BenchmarkNewPerson(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		NewPerson(i)
	}
}

func BenchmarkPerson_Read(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()

	p := Person{PersonId: 1}
	for i := 0; i < b.N; i++ {
		p.Read(l.Books[0])
	}
}

func BenchmarkPerson_TakeBook(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		p := Person{
			PersonId:        1,
			СompletedBooks:  make([]int, 0, l.GetCountBook()),
			LastWordsInBook: make([]ReadableBooks, 0, 30),
		}
		c := make(chan *Person, 1)
		ctx, _ := context.WithTimeout(context.Background(), time.Second)
		p.TakeBook(l, ctx, c)
	}
}
