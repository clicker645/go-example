package person

import (
	"context"
	"go-example/library/book"
	"go-example/library/library_dir"
	"sync"
)

type Person struct {
	sync.Mutex

	PersonId        int
	ReadAllBooks    bool
	СompletedBooks  []int
	LastWordsInBook []ReadableBooks
}

type ReadableBooks struct {
	BookId int
	Word   int
}

func NewPerson(personId int) *Person {
	return &Person{
		PersonId: personId,
	}
}

func CheckNewBook(s []ReadableBooks, i int) bool {
	for _, v := range s {
		if v.BookId == i {
			return false
		}
	}

	return true
}

func (p *Person) Read(book *book.Book) {
	if p.LastWordsInBook == nil {
		p.LastWordsInBook = append(p.LastWordsInBook, ReadableBooks{BookId: book.BookId, Word: 1})
	} else if CheckNewBook(p.LastWordsInBook, book.BookId) {
		p.LastWordsInBook = append(p.LastWordsInBook, ReadableBooks{BookId: book.BookId, Word: 1})
	} else {
		for index, value := range p.LastWordsInBook {
			if book.BookId == value.BookId {
				p.LastWordsInBook[index].Word++
				if p.LastWordsInBook[index].Word == len(book.Text) {
					p.СompletedBooks = append(p.СompletedBooks, book.BookId)
				}
				break
			}
		}
	}
}

func (p *Person) TakeBook(l *library_dir.Library, ctx context.Context, winner chan *Person) {
LOOP:
	for {
		if len(p.СompletedBooks) == l.GetCountBook() {
			l.Lock()
			p.ReadAllBooks = true
			winner <- p
			break LOOP
		}

		select {
		case <-ctx.Done():
			break LOOP
		default:
			newBook := l.GiveBook(p.PersonId, p.СompletedBooks)
			if newBook == nil {
				continue
			}

			p.Read(newBook)
			l.ReturnBook(newBook.BookId, p.PersonId)
		}
	}

}
