package main

import (
	"context"
	"fmt"
	"go-example/library/library_dir"
	"go-example/library/person"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
)

const (
	CountReaders      = 3
	BookCount         = 6
	StartIndexingBook = 1
)

func main() {
	cpuProfile, err := os.Create("example-cpu.prof")
	if err != nil {
		log.Fatal(err)
	}

	pprof.StartCPUProfile(cpuProfile)
	defer pprof.StopCPUProfile()

	l := &library_dir.Library{}
	l.CreateBooks(StartIndexingBook, BookCount)

	ctx, cancel := context.WithCancel(context.Background())
	winner := make(chan *person.Person, 1)

	for i := 1; i <= CountReaders; i++ {
		go func(ctx context.Context, i int, l *library_dir.Library, c chan *person.Person) {
			p := person.NewPerson(i)
			p.TakeBook(l, ctx, winner)
		}(ctx, i, l, winner)
	}

	fmt.Println("WINNER", <-winner)
	close(winner)
	cancel()
	l.Unlock()

	runtime.GC()
	memProfile, err := os.Create("example-mem.prof")
	if err != nil {
		log.Fatal(err)
	}

	defer memProfile.Close()

	if err := pprof.WriteHeapProfile(memProfile); err != nil {
		log.Fatal(err)
	}
}
