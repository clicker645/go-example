package book

const (
	BookName   = "book"
	BookFormat = "txt"
)

type Book struct {
	BookId int
	Text   []byte
	Reader int
}

func NewBook(bookId int, text []byte) *Book {
	return &Book{
		BookId: bookId,
		Text:   text,
	}
}
