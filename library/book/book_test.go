package book

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var Tests = []struct {
	BookId   int
	BookText []byte
}{
	{1, []byte("some 1")},
	{2, []byte("some 2")},
	{3, []byte("some 3")},
	{4, []byte("some 4")},
	{5, []byte("some 5")},
}

func TestNewBook(t *testing.T) {
	for idx, test := range Tests {
		book := NewBook(test.BookId, test.BookText)
		assert.Equal(t, book.BookId, book.BookId, "Test failed-> expected: %v, received: %v", Tests[idx], book)
		assert.Equal(t, string(book.Text), string(test.BookText), "Test failed-> expected: %v, received: %v", Tests[idx], book)
	}
}

func BenchmarkNewBook(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		NewBook(1, []byte{12, 12, 12})
	}
}
