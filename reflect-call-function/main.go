package main

import (
	"fmt"
	"reflect"
)

func Call(fn interface{}, args ...interface{}) {
	v := reflect.ValueOf(fn)
	if v.Kind() != reflect.Func {
		panic("It`s not a function")
	}

	argsValue := reflect.ValueOf(args)
	argv := make([]reflect.Value, argsValue.Len())
	for i, v := range args {
		argv[i] = reflect.ValueOf(v)
	}

	v.Call(argv)

}

type User struct {
	name string
}

func main() {

	var mapFn = make(map[string]interface{})

	mapFn["some"] = func(i int, a int, b int, s string) {
		fmt.Println("some", i, a, b, s)
	}
	mapFn["user"] = func(user User) {
		fmt.Println("name", user.name)
	}
	mapFn["user-pointer"] = func(user *User) {
		fmt.Println("name", user.name)
	}

	Call(mapFn["some"], 1, 2, 3, "VALUE")
	Call(mapFn["user"], User{"Bob"})
	Call(mapFn["user-pointer"], &User{"John"})
}
