package main

import (
	"context"
	"fmt"
	"log"
	"time"
)

type funcType func()

func setInterval(f funcType, timeInterval time.Duration) context.CancelFunc {
	ctx, cancel := context.WithCancel(context.Background())
	go func(c context.Context) {
		for {
			select {
			case <-time.Tick(time.Second * timeInterval):
				f()
			case <-c.Done():
				log.Print(c.Err())
				return
			}
		}
	}(ctx)

	return cancel
}

func setTimeout(f funcType, timeOut time.Duration) context.CancelFunc {
	ctx, cancel := context.WithCancel(context.Background())
	go func(c context.Context) {
		select {
		case <-time.After(time.Second * timeOut):
			f()
		case <-c.Done():
			log.Print(c.Err())
		}
	}(ctx)

	return cancel
}

func clearInterval(clear context.CancelFunc) {
	clear()
}

func main() {
	cancel := setTimeout(func() { fmt.Println("hello") }, 2)
	//cancel := setInterval(func() { fmt.Println("hello") }, 1)
	setTimeout(func() { clearInterval(cancel) }, 3)

	fmt.Scanln()
}
