package main

import (
	"fmt"
	"time"
)

type funcType func()

func setInterval(f funcType, timeInterval time.Duration) chan bool {
	c := make(chan bool, 1)
	go func(c chan bool) {
		for {
			select {
			case <-c:
				return
			case <-time.Tick(time.Millisecond * timeInterval):
				f()
			}
		}
	}(c)
	return c
}

func setTimeout(f funcType, timeOut time.Duration) chan bool {
	c := make(chan bool, 1)
	go func(c chan bool) {
		select {
		case <-c:
			return
		case <-time.After(time.Second * timeOut):
			f()
		}
	}(c)

	return c
}

func clearInterval(c chan bool) {
	close(c)
}

func main() {

	c := setInterval(func() { fmt.Println("Hello") }, 500)
	setTimeout(func() { clearInterval(c) }, 3)

	time.Sleep(time.Second * 4)

	fmt.Println("done")
}
