package main

import (
	"fmt"
	"time"
)

func worker(jobs <-chan int, results chan<- int) {
	for j := range jobs {
		time.Sleep(time.Second)
		results <- fibonacciSearch(j)
	}
	fmt.Println("close")
}

func fibonacciSearch(n int) int {
	if n <= 2 {
		return 1
	}
	return fibonacciSearch(n-2) + fibonacciSearch(n-1)
}

func main() {

	mas := []int{
		5, 6, 7, 8, 9,
		5, 6, 7, 8, 9,
		5, 6, 7, 8, 9,
		5, 6, 7, 8, 9,
		5, 6, 7, 8, 9,
	}

	jobs := make(chan int, 100)
	results := make(chan int, 100)

	for _, value := range mas {
		jobs <- value
	}

	for w := 1; w <= 10; w++ {
		go worker(jobs, results)
	}

	close(jobs)

	for a := 1; a <= len(mas); a++ {
		fmt.Println(<-results)
	}

	time.Sleep(time.Second * 5)
}
