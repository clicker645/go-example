package helpers

import "os"

func OpenOrCreateFile(pathToFile string) (*os.File, error) {
	file, errOpenFile := os.OpenFile(pathToFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if errOpenFile != nil {
		return nil, errOpenFile
	}

	return file, nil
}
