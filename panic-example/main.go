package main

import (
	"fmt"
	"go-example/panic-example/helpers"
	"go-example/panic-example/logger"
	"log"
	"runtime/debug"
)

const (
	fileLogsPath = "logs.txt"
	errorMessage = "example error message"
)

var lg *logger.Logger

func CallPanic() (er error) {
	defer func() {
		if r := recover(); r != nil {
			lg.AddLog(string(debug.Stack()))
			er = fmt.Errorf("%v", r)
		}
	}()

	panic(errorMessage)
}

func main() {
	f, err := helpers.OpenOrCreateFile(fileLogsPath)
	if err != nil {
		log.Fatal(err)
	}

	lg = logger.NewLogger(f)
	fmt.Println(CallPanic())
}
