package logger

import (
	"io"
	"log"
	"time"
)

type Logger struct {
	logger *log.Logger
}

func (l *Logger) AddLog(message string) error {
	l.logger.Println(time.Now().String() + "\n" + message + "\n\n")

	return nil
}

func NewLogger(out io.Writer) *Logger {
	return &Logger{log.New(out, "", log.LstdFlags)}
}
